<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('fron', function(){
    return view ('frontend.index');
});
Route::get('tes', function(){
    return view ('frontend.tes');
});

Route::get('data', function(){
    return view ('frontend.data');
});

Route::get('seeder', function(){
    return view ('frontend.seeder');
});

Route::get('admin', function(){
    return view('backend.master');
})->name('admin');

Route::get('btc', 'CryptoController@getbtc');
Route::get('eth', 'CryptoController@geteth');
Route::get('gold', 'CryptoController@getgold');

Route::get('upeth', 'CryptoController@update_eth');

Route::get('upbtc', 'CryptoController@update_btc');

Route::get('upgold', 'CryptoController@update_gold');

Route::get('a', function(){
    return "hallo";
});
