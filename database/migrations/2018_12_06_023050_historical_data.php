<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HistoricalData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_btc', function (Blueprint $table) {
            $table->increments('id');
            $table->string('epoch');
            $table->string('value');
            $table->timestamps();
        });

        Schema::create('history_eth', function (Blueprint $table) {
            $table->increments('id');
            $table->string('epoch');
            $table->string('value');
            $table->timestamps();
        });

        Schema::create('history_gold', function (Blueprint $table) {
            $table->increments('id');
            $table->string('epoch');
            $table->string('value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_btc');
        Schema::dropIfExists('history_eth');
        Schema::dropIfExists('history_gold');
    }
}
