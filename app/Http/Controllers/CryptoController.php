<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\History_btc;
use App\History_eth;
use App\History_gold;

class CryptoController extends Controller
{

    public function getbtc(){
        // $data = History_btc::select('epoch', 'value')->get();
        //
        // // return $data;
        // return response()->json($data);

        // echo "[<br>";
        // for ($i=0; $i<count($data); $i++) {
        //   $d1 = $data[$i]["epoch"];
        //   $d2 =  $data[$i]["value"];
        //   echo "[<br>'epoch' => '".$d1 ."',<br>'value' => '".$d2."',<br>],<br>";
        // }
        // echo "];";

        // echo "[";
        // for ($i=0; $i<count($data); $i++) {
        //   echo "[".$data[$i]["epoch"] .",".$data[$i]["value"]."],<br>";
        //
        // }
        // echo "]";


        $result = [];
        foreach (History_btc::all() as $data) {
          $result[] = [$data->epoch, $data->value];
        }

        return $result;

        $fp = fopen('tes.json', 'w');
        fwrite($fp, json_encode($result));
        fclose($fp);

    }

    public function geteth(){
          $data = History_eth::select('epoch', 'value')->get();

          echo "[";
          for ($i=0; $i<count($data); $i++) {
            echo "[".$data[$i]["epoch"] .",".$data[$i]["value"]."],<br>";

          }
          echo "]";
    }

    public function getgold(){
          $data = History_gold::select('epoch', 'value')->get();

          $kurs = file_get_contents('https://kurs.web.id/api/v1/bi');
          $k = json_decode($kurs, true);

          echo "[";
          for ($i=0; $i<count($data); $i++) {
            $gold =  ($data[$i]["value"])/$k["jual"];
            echo "[".$data[$i]["epoch"] .",".($gold*10)."],<br>";

          }
          echo "]";
    }

    public function update_eth(){
        $latest = History_eth::latest('epoch')->first();

        $json = file_get_contents('https://poloniex.com/public?command=returnChartData&currencyPair=USDT_ETH&start=1439006400&end=9999999999&period=14400');
        $obj = json_decode($json, true);

        for ($i=0; $i<count($obj); $i++) {
            if ( $obj[$i]["date"] == $latest['epoch']/1000 ) {
                if($obj[$i]["close"] != $latest['value']){
                    // echo "tidak sama";

                    $latest->value = $obj[$i]["close"];
                    $latest->save();
                }
                $index = $i+1;
                break;
            }
        }


        // echo "[<br>";
        for ($i=$index; $i<count($obj); $i++) {
          $data = new History_eth([
                'epoch' => ($obj[$i]["date"])*1000,
                'value' => $obj[$i]["close"]
          ]);
          $data->save();
          // $date = ($obj[$i]["date"])*1000;
          // echo "[<br>'epoch' => '".$date ."',<br>'value' => '".$obj[$i]["close"]."',<br>],<br>";
        }
        // echo "];";
    }

    public function update_btc(){
        $latest = History_btc::latest('epoch')->first();

        $json = file_get_contents('https://poloniex.com/public?command=returnChartData&currencyPair=USDT_BTC&start=1424361600&end=9999999999&period=14400');
        $obj = json_decode($json, true);

        for ($i=0; $i<count($obj); $i++) {
            if ( $obj[$i]["date"] == $latest['epoch']/1000 ) {
                if($obj[$i]["close"] != $latest['value']){
                    // echo "tidak sama";

                    $latest->value = $obj[$i]["close"];
                    $latest->save();
                }
                $index = $i+1;
                // echo $index;
                break;
            }
        }


        // echo "[<br>";
        for ($i=$index; $i<count($obj); $i++) {
          $data = new history_btc([
                'epoch' => ($obj[$i]["date"])*1000,
                'value' => $obj[$i]["close"]
          ]);
          $data->save();
          // $date = ($obj[$i]["date"])*1000;
          // echo "[<br>'epoch' => '".$date ."',<br>'value' => '".$obj[$i]["close"]."',<br>],<br>";
        }
        // echo "];";
    }


    public function update_gold(){
        $latest = History_gold::latest('epoch')->first();

        $json = file_get_contents('http://dinarworld.com/rsgold/index.php/api/graph/emas?igrow=03edde974362db42832b85a7144ca7d0');
        $obj = json_decode($json, true);

        for ($i=0; $i<count($obj); $i++) {
            if ( $obj[$i]["tanggal"] == $latest['epoch']/1000 ) {
                if($obj[$i]["goldpergr"] != $latest['value']){
                    // echo "tidak sama";

                    $latest->value = $obj[$i]["goldpergr"];
                    $latest->save();
                }
                $index = $i+1;
                // echo $index;
                break;
            }
        }



      // echo "[<br>";
      for ($i=$index; $i<count($obj); $i++) {
        $data = new history_gold([
              'epoch' => ($obj[$i]["tanggal"])*1000,
              'value' => $obj[$i]["goldpergr"]
        ]);
        $data->save();
        // $date = ($obj[$i]["tanggal"])*1000;
        // echo "[<br>'epoch' => '".$date ."',<br>'value' => '".$obj[$i]["goldpergr"]."',<br>],<br>";
      }
      // echo "];";
    }

}
