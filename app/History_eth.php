<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History_eth extends Model
{
  protected $table = 'history_eth';
  protected $fillable = [
      'epoch', 'value'
  ];
}
