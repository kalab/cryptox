<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History_btc extends Model
{
  protected $table = 'history_btc';
  protected $fillable = [
      'epoch', 'value'
  ];
}
