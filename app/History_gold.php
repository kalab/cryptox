<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History_gold extends Model
{
  protected $table = 'history_gold';
  protected $fillable = [
      'epoch', 'value'
  ];
}
