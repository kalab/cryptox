<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>CryptoX</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Bootstrap Core Css -->
    @section('css')
        {{ Html::style('backof/css/bootstrap.css') }}
        {{ Html::style('backof/css/waves.css') }}
        {{ Html::style('backof/css/animate.css') }}
        {{ Html::style('backof/css/morris.css') }}
        {{ Html::style('backof/css/style.css') }}
        {{ Html::style('backof/css/all-themes.css') }}

         <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    @show

    @yield('extra-css')
</head>

<body class="theme-blue-grey">
    @include('backend.partials.loader')
    <div class="overlay"></div>
    @include('backend.partials.header')
    @include('backend.partials.sidebar')

    <section class="content">
        @yield('content')
    </section>

    @section('script')
        {{Html::script('backof/js/jquery.min.js')}}
        {{Html::script('backof/js/bootstrap.js')}}
        {{Html::script('backof/js/bootstrap-select.js')}}
        {{Html::script('backof/js/jquery.slimscroll.js')}}
        {{Html::script('backof/js/waves.js')}}

    @show
    @yield('extra-script')
    @section('script-bottom')
        {{Html::script('backof/js/admin.js')}}
        {{Html::script('backof/js/demo.js')}}
    @show
</body>

</html>
